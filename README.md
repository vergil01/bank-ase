# Test-Driven Development (TDD)

### Clone the project from BitBucket
1. Open Visual Studio, navigate to File/New/Repository. The Team Explorer window will open up.
2. Under "Local Git Repositories", select "Clone".
3. Enter the following URL: `https://vergil01@bitbucket.org/vergil01/bank-ase.git`
4. Enter the destination folder (i.e. C:\Users\{YOU}\Source\Repos\Bank)
5. Select View/Solution Explorer. Check that the project has been cloned correctly and you can see the files.
6. Select the Solution and click on Build/Build Solution.

### Task
Your task is to implement two simple operations for the BankAccount class: `credit()` and `debit()`.
You will use a test-driven approach:

* Tests are written before the implementation.
* When first run, all tests are expected to fail.
* Implement one feature at a time, writing just enough code to pass the tests.
* Re-run the tests until they all pass.
* Refactor the code, making sure the tests still pass.

### Steps
1. Navigate to Test/Windows/Test Explorer to open the Test Explorer.
2. From the top of the Test Explorer, click on "Run All". You should see the results indicating that all your tests have failed.
3. Open the BankAccountTests.cs class to look at the first test: `Credit_WithValidAmount_UpdatesBalance()`. Understand what the test is trying to do.
4. Open the BankAccount.cs class and write a simple implementation - just enough to pass the the test.
5. From your Test Explorer, click on "Run All" again. You will see that at least one of the tests is now green. If they are all still red, change the implementation again until the test passes.
6. Repeat steps 3-4 for the `Credit_WithNegativeAmount_ThrowsException()` and `Credit_FrozenAccount_ThrowsException()` tests. Hint: you will need to check for a condition in your implementation and throw an exception if that condition is true. 
7. If the `Credit_WithZeroAmount_DoesNotUpdateBalance()` test is still red (it probably won't be,  it depends on how you implemented step 3), repeat steps 3-4 for it to make sure it passes.
8. Now it's your turn to write your own tests and then write the implementation code. Follow the examples given for the credit operation and implement the reverse: a debit operation. Make sure you use the test-driven approach outlined at the start of this document.

### Have fun!
